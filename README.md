# SocialBankDesafio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Setup

- Install Node.js `https://nodejs.org/en/download/`

- Install Angular CLI `npm install -g @angular/cli`

- Install Node Packages `npm install`

- Install Json-Server `npm install -g json-server`

## API Json Server

Run `json-server --watch db.json`. Add  `--delay 2000` to test loading proccess.
(It must be on a different Terminal of Development server)



## Development server

Run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.
The app will automatically reload if you change any of the source files.

import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVICE_URL } from 'src/app/app.constants';
import { User } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public activeUser: User | null = null;

  private userUrl = SERVICE_URL;
  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl + 'users');
  }

  getUserById(userId: string): Observable<HttpResponse<User>> {
    return this.http.get<HttpResponse<User>>(this.userUrl + 'users/' + userId);
  }

  createUser(user: User): Observable<HttpResponse<User>> {
    return this.http.post<HttpResponse<User>>(this.userUrl + 'users', user);
  }

  updateUser(user: User): Observable<HttpResponse<User>> {
    return this.http.put<HttpResponse<User>>(
      this.userUrl + 'users/' + user.id,
      user
    );
  }
  deleteUserById(userId: string): Observable<HttpResponse<User>> {
    return this.http.delete<HttpResponse<User>>(
      this.userUrl + 'users/' + userId
    );
  }
}

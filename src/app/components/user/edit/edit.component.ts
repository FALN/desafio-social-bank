import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StatesList } from 'src/app/utils/statesList';
import { GenericValidator } from 'src/app/utils/validation-utils';
import { ToastService, toastType } from '../../toast-message/toast.service';
import { IUser, User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  isLoading = false;
  editForm = this.formBuilder.group({
    id: [null, [Validators.required]],
    name: [
      null,
      [
        Validators.required,
        GenericValidator.noWhitespaceValidator,
        Validators.pattern('^[a-zA-Zà-úÀ-Ú ]*$'),
      ],
    ],
    email: [null, [Validators.required, Validators.email]],
    phone: [null, [Validators.required, GenericValidator.isValidPhone]],
    birthdate: [null, [Validators.required]],

    postcode: [null, [Validators.required]],
    street: [null, [Validators.required]],
    district: [null, [Validators.required]],
    city: [null, [Validators.required]],
    state: [null, [Validators.required]],
  });

  statesList = StatesList;
  cidadeList: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    public toastService: ToastService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const userId = this.route.snapshot.paramMap.get('id');
    if (userId) {
      this.isLoading = true;
      this.userService.getUserById(userId).subscribe(
        (suc: any) => {
          this.updateForm(suc);
          this.setCidadeValidators();
          this.updateCidadeList();
          this.isLoading = false;
        },
        (err: any) => {
          console.warn(err);
          this.isLoading = false;
          this.router.navigate(['/users']);
          this.toastService.sendMessage({
            type: toastType.warn,
            message: 'Usuário não encontrado',
          });
        }
      );
    } else {
      this.router.navigate(['/users']);
    }
  }

  updateForm(user: IUser): void {
    this.editForm.patchValue({
      id: user.id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      birthdate: user.birthdate,
      postcode: user.address?.postcode,
      street: user.address?.street,
      district: user.address?.district,
      city: user.address?.city,
      state: user.address?.state,
    });
  }

  previousPage(): void {
    this.router.navigate([
      '/users/view/' + this.route.snapshot.paramMap.get('id'),
    ]);
  }

  setCidadeValidators(): void {
    this.editForm.get('state')!.valueChanges.subscribe((uf) => {
      this.editForm.get('city')?.setValue(null);
      this.updateCidadeList();
    });
  }

  updateCidadeList(): void {
    if (this.editForm.get('state')?.value) {
      const found = this.statesList.find(
        (stateList) => stateList.nome === this.editForm.get('state')?.value
      );
      if (found) {
        this.cidadeList = found.cidades;
      }
    } else {
      this.cidadeList = [];
    }
  }

  save(): void {
    if (this.editForm.valid) {
      this.isLoading = true;
      const finalUser = this.createFinalUser();
      console.warn(finalUser);
      this.userService.updateUser(finalUser).subscribe(
        (suc) => {
          this.isLoading = false;
          this.toastService.sendMessage({
            type: toastType.success,
            message: 'Usuário atualizado com sucesso',
          });
        },
        (err) => {
          console.warn(err);
          this.isLoading = false;
          this.toastService.sendMessage();
        }
      );
    } else {
      this.toastService.sendMessage({
        type: toastType.warn,
        message: 'Confira os campos antes de continuar',
      });
      this.markFormGroupTouched(this.editForm);
    }
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  createFinalUser(): IUser {
    return {
      id: this.editForm.get('id')?.value,
      name: this.editForm.get('name')?.value,
      email: this.editForm.get('email')?.value,
      phone: this.editForm.get('phone')?.value,
      birthdate: this.editForm.get('birthdate')?.value,
      address: {
        postcode: this.editForm.get('postcode')?.value,
        street: this.editForm.get('street')?.value,
        district: this.editForm.get('district')?.value,
        city: this.editForm.get('city')?.value,
        state: this.editForm.get('state')?.value,
      },
    };
  }
}

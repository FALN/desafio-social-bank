import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { RouterModule } from '@angular/router';
import { usersRoute } from './user.route';
import { ViewComponent } from './view/view.component';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IConfig, NgxMaskModule } from 'ngx-mask';
const maskConfig: Partial<IConfig> = { validation: false };

@NgModule({
  declarations: [ListComponent, EditComponent, CreateComponent, ViewComponent],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatInputModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    CommonModule,
    NgxMaskModule.forRoot(maskConfig),
    RouterModule.forChild(usersRoute),
  ],
})
export class UsersModule {}

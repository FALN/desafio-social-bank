import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StatesList } from 'src/app/utils/statesList';
import { GenericValidator } from 'src/app/utils/validation-utils';
import { ToastService, toastType } from '../../toast-message/toast.service';
import { IUser } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  isLoading = false;
  createForm = this.formBuilder.group({
    name: [
      null,
      [
        Validators.required,
        GenericValidator.noWhitespaceValidator,
        Validators.pattern('^[a-zA-Zà-úÀ-Ú ]*$'),
      ],
    ],
    email: [null, [Validators.required, Validators.email]],
    phone: [null, [Validators.required, GenericValidator.isValidPhone]],
    birthdate: [null, [Validators.required]],

    postcode: [null, [Validators.required]],
    street: [null, [Validators.required]],
    district: [null, [Validators.required]],
    city: [null, [Validators.required]],
    state: [null, [Validators.required]],
  });

  statesList = StatesList;
  cidadeList: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.setCidadeValidators();
    this.updateCidadeList();
  }

  previousPage(): void {
    this.router.navigate(['users']);
  }

  setCidadeValidators(): void {
    this.createForm.get('state')!.valueChanges.subscribe((uf) => {
      this.createForm.get('city')?.setValue(null);
      this.updateCidadeList();
    });
  }

  updateCidadeList(): void {
    if (this.createForm.get('state')?.value) {
      const found = this.statesList.find(
        (stateList) => stateList.nome === this.createForm.get('state')?.value
      );
      if (found) {
        this.cidadeList = found.cidades;
      }
    } else {
      this.cidadeList = [];
    }
  }

  save(): void {
    if (this.createForm.valid) {
      this.isLoading = true;
      const finalUser = this.createFinalUser();
      console.warn(finalUser);
      this.userService.createUser(finalUser).subscribe(
        (suc) => {
          this.isLoading = false;
          this.toastService.sendMessage({
            type: toastType.success,
            message: 'Novo usuário criado com sucesso',
          });
          this.createForm.reset();
        },
        (err) => {
          console.warn(err);
          this.isLoading = false;
          this.toastService.sendMessage();
        }
      );
    } else {
      this.toastService.sendMessage({
        type: toastType.warn,
        message: 'Confira os campos antes de continuar',
      });
      this.markFormGroupTouched(this.createForm);
    }
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  createFinalUser(): IUser {
    return {
      name: this.createForm.get('name')?.value,
      email: this.createForm.get('email')?.value,
      phone: this.createForm.get('phone')?.value,
      birthdate: this.createForm.get('birthdate')?.value,
      address: {
        postcode: this.createForm.get('postcode')?.value,
        street: this.createForm.get('street')?.value,
        district: this.createForm.get('district')?.value,
        city: this.createForm.get('city')?.value,
        state: this.createForm.get('state')?.value,
      },
    };
  }
}

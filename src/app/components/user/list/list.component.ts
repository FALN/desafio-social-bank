import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastService, toastType } from '../../toast-message/toast.service';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  displayedColumns: string[] = ['name', 'email', 'phone', 'birthdate'];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<User> = new MatTableDataSource([{}]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  isLoadingResults = false;
  constructor(
    private router: Router,
    public userService: UserService,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.responsiveTableCheck();
    this.loadData();
  }

  loadData(): void {
    this.isLoadingResults = true;
    this.userService.getAllUsers().subscribe(
      (suc: User[]) => {
        console.warn(suc);
        this.dataSource = new MatTableDataSource(suc);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isLoadingResults = false;
      },
      (err: any) => {
        console.warn(err);
        this.toastService.sendMessage();
        this.isLoadingResults = false;
      }
    );
  }

  responsiveTableCheck(): void {
    const screenWidth = screen.width;

    if (screenWidth <= 600) {
      this.columnsToDisplay = ['name', 'email'];
    } else if (screenWidth > 600 && screenWidth <= 900) {
      this.columnsToDisplay = ['name', 'email', 'phone'];
    } else {
      this.columnsToDisplay = ['name', 'email', 'phone', 'birthdate'];
    }

    window.matchMedia('(max-width: 600px)').addEventListener('change', (e) => {
      if (e.matches) {
        this.columnsToDisplay = ['name', 'email'];
      }
    });
    window
      .matchMedia('(min-width:601px) and (max-width: 900px)')
      .addEventListener('change', (e) => {
        if (e.matches) {
          this.columnsToDisplay = ['name', 'email', 'phone'];
        }
      });

    window.matchMedia('(min-width:901px)').addEventListener('change', (e) => {
      if (e.matches) {
        this.columnsToDisplay = ['name', 'email', 'phone', 'birthdate'];
      }
    });
  }

  createNewUser(): void {
    this.router.navigate(['/users/create']);
  }

  userRowClicked(user: User) {
    this.router.navigate(['/users/view/' + user.id]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource!.filter = filterValue.trim().toLowerCase();

    if (this.dataSource!.paginator) {
      this.dataSource!.paginator.firstPage();
    }
  }
}

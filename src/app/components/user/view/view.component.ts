import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogConfirmComponent } from '../../dialog-confirm/dialog-confirm.component';
import { ToastService, toastType } from '../../toast-message/toast.service';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {
  isLoading = false;

  currentUser?: User;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public toastService: ToastService,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    const userId = this.route.snapshot.paramMap.get('id');
    if (userId) {
      this.isLoading = true;
      this.userService.getUserById(userId).subscribe(
        (suc: any) => {
          this.currentUser = suc;
          this.isLoading = false;
        },
        (err: any) => {
          console.warn(err);
          this.isLoading = false;
          this.router.navigate(['/users']);
          this.toastService.sendMessage({
            type: toastType.warn,
            message: 'Usuário não encontrado',
          });
        }
      );
    } else {
      this.router.navigate(['/users']);
    }
  }

  editUser(): void {
    this.router.navigate([
      '/users/edit/' + this.route.snapshot.paramMap.get('id'),
    ]);
  }

  previousPage(): void {
    this.router.navigate(['/users']);
  }

  deleteUser(): void {
    const dialogRefConfirm = this.dialog.open(DialogConfirmComponent, {
      maxHeight: '90vh',
      disableClose: true,
      autoFocus: false,
      data: 'o usuário ' + this.currentUser!.name,
    });
    dialogRefConfirm.afterClosed().subscribe((result) => {
      if (result === true) {
        this.confirmedDeleteUser();
      }
    });
  }

  confirmedDeleteUser(): void {
    const userId = this.route.snapshot.paramMap.get('id');
    if (userId) {
      this.isLoading = true;
      this.userService.deleteUserById(userId).subscribe(
        (suc: any) => {
          console.warn(suc);
          this.isLoading = false;
          this.toastService.sendMessage({
            type: toastType.success,
            message: 'Usuário excluído com sucesso',
          });
          this.router.navigate(['/users']);
        },
        (err: any) => {
          console.warn(err);
          this.isLoading = false;
          this.router.navigate(['/users']);
          this.toastService.sendMessage({
            type: toastType.warn,
            message: 'Usuário não encontrado',
          });
        }
      );
    }
  }
}

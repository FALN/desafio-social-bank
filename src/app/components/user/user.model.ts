export interface IUser {
  id?: number;
  name?: string;
  email?: string;
  phone?: number;
  birthdate?: string;
  address?: Address;
}

export class User implements IUser {
  constructor(
    public id?: number,
    public name?: string,
    public email?: string,
    public phone?: number,
    public birthdate?: string,
    public thumb?: string,
    public address?: Address
  ) {}
}

export interface Address {
  postcode?: string;
  street?: string;
  district?: string;
  city?: string;
  state?: string;
}

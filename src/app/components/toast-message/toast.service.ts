import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

export enum toastType {
  error = 'error',
  success = 'success',
  warn = 'warn',
}

export interface IToast {
  type: toastType;
  message: string;
}

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private _snackBar: MatSnackBar) {}

  // this.toastService.sendMessage({type: toastType.success, message: 'foi '});
  // this.toastService.sendMessage();
  public sendMessage(toast?: IToast): void {
    if (!toast) {
      toast = {
        type: toastType.error,
        message: 'Houve algum erro, por favor, tente novamente',
      };
    }
    this._snackBar.open(toast.message, 'OK', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: [this.returnClassByType(toast.type)],
    });
  }

  private returnClassByType(type: toastType): string {
    if (type == toastType.error) {
      return 'error-snackbar';
    }
    if (type == toastType.warn) {
      return 'warn-snackbar';
    }
    if (type == toastType.success) {
      return 'success-snackbar';
    }
    return '';
  }
}
